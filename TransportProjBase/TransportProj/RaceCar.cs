﻿using System;

namespace TransportProj
{
    public class RaceCar : Car
    {
        public RaceCar(int xPos, int yPos, City city, Passenger passenger)
            : base(xPos, yPos, city, passenger)
        {
        }


        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos += 2;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos -= 2;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos += 2;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos -= 2;
                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("RaceCar moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
